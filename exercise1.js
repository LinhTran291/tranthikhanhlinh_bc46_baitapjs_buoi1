/**
 * Mô hình 3 khối:
 * Đầu vào: Nhập số ngày
 * Xử lý: tiền lương = số ngày * lương 1 ngày
 * Đầu ra: tiền lương
 */
function tinhtienluong() {
  var soNgay = document.getElementById("soNgay").value * 1;
  console.log("Số ngày",soNgay);
  var tienLuong = 100000 * soNgay;
  console.log("Tiền lương",tienLuong);
  document.getElementById("alert1").style.visibility = "visible";
  document.getElementById(
    "alert1"
  ).innerHTML = `Tiền lương của nhân viên làm trong ${soNgay} là: ${tienLuong} VNĐ`;
}
/**
 * Mô hình 3 khối: 
 * Đầu vào: 5 đầu vào - 5 số
 * Xử lý: B1: Tính tổng 5 số
 *        B2: Lấy tổng / 5
 * Đầu ra: trung bình cộng của 5 số
 */
function tinhtrungbinh() {
  var soThuNhat = document.getElementById("soThuNhat").value * 1;
  var soThuHai = document.getElementById("soThuHai").value * 1;
  var soThuBa = document.getElementById("soThuBa").value * 1;
  var soThuTu = document.getElementById("soThuTu").value * 1;
  var soThuNam = document.getElementById("soThuNam").value * 1;
  // console.log("Số thứ 1: ",soThuNhat, "Số thứ 2: ", soThuHai , "Số thứ 3: ", soThuBa ,"Số thứ 4: ", soThuTu , "Số thứ 5: ", soThuNam);
  var trungBinh = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5;
  // console.log("Trung binh: ", trungBinh);
  document.getElementById("alert2").style.visibility = "visible";
  document.getElementById(
    "alert2"
  ).innerHTML = `Gía trị trung bình của 5 số là: ${trungBinh}`;
}
/**
 * Mô hình 3 khối:
 * Đầu vào: số tiền với đơn vị là USD
 * Xử lý: Tiền qui đổi=23.500* số tiền
 * Đầu ra: số tiền vnđ có được khi đổi ra từ tiền usd
 */
function quidoi() {
  var soTien = document.getElementById("soTien").value * 1;
  var tienQuiDoi = soTien * 23500;
  document.getElementById("alert3").style.visibility = "visible";
  document.getElementById(
    "alert3"
  ).innerHTML = `Kết quả qui đổi ${soTien} được ${tienQuiDoi} VNĐ `;
}
/**
 * Đầu vào: chiều dài, chiều rộng hình chữ nhật
 * Xử lí: diện tích = dài*rộng
 *        chu vi = (dài + rộng)/2
 * Đầu ra: chu vi và diện tích hình chữ nhật
 */
function tinhdientich() {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;
  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) / 2;
  document.getElementById("alert4").style.visibility = "visible";
  document.getElementById(
    "alert4"
  ).innerHTML = `Diện tích hình chữ nhật là: ${dienTich} <br /> Chu vi hình chữ nhật là: ${chuVi}`;
}
/**
 * Đầu vào: số có hai chữ số
 * Xử lý - Chữ số hàng chục = Math.floor(số có hai chữ số /10)
 *       - Chữ số hàng trăm = (số có hai chữ số)%10;
 * Đầu ra: Kết quả tổng các số trong số hai chữ số
 * 
 */
function tinhtongsohaiso() {
  var soHaiSo = document.getElementById("soHaiSo").value * 1;

  var tongSo = Math.floor(soHaiSo / 10) + (soHaiSo % 10);
  document.getElementById("alert5").style.visibility = "visible";
  document.getElementById(
    "alert5"
  ).innerHTML = `Tổng các chữ số là : ${tongSo}`;
}
